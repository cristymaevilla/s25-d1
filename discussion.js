// AGGREGATION IN MONGODB:===========================================
/*  - aggregation group values from mutiple documents together
MONGODB AGGREGATION:
		-used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
e.g. 
-which students have not finished their courses yet
-count the total num. of courses the student has complete
-determine no. of students who finished a specified course

*/
// create fruits document:
db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale":true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Banana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale":true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale":true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale":false,
		"origin": ["Philippines", "India"]
	}
]);

// AGGREGATE METHOD 
/** $match
		-to pass docs. that meets the specific consdition/s top the next piepline stage/aggregation process.
	SYNTAX:
	{$match : {field:value} }

$group
		-group elements together and field-value pairs using the data from the grouped elements
	SYNTAX:
	{$group: {_id: "value", fieldResult: "valueResult"}}
*/

// aggregate to fnd products on sale and group the total stocks
db.fruits.aggregate	([
		{ $match: {"onSale": true } },
		{ $group: {"_id": "$supplier_id", "total": {$sum: "$stock"}}}
]);
// result: id 1,20+25 stocks =45; id 2,20 stocks


// SORTING AGGREGATED RESULTS------------
/*  $sort
		- used to change the order of aggregated result
   SYNTAX: (1:proper/increasing, -1:reverse/decreasing)
   	{$sort {field: 1/-1} }
*/
 
db.fruits.aggregate([
		{ $match: {"onSale": true} },
		{ $group: {"_id": "$supplier_id", "total": {$sum: "$stock"}} },
		{$sort: {"total": -1} }
]);

// AGGREGATE RESULTS BASED ON ARRAY FIELDS---------------------------------------
/* $unwind
		-deconstruct an array field from a collection/field with an array value to output a result for each element
		-deconstruct a field that holds an array
	SYNTAX:
	{ $unwind : field }
*/

// will count how many countries in a document: shows country one by one

db.fruits.aggregate([
		{$unwind: "$origin"}
]);



// deconstruct, group and sum the kinds of fruit per country

db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group: {_id: "$origin", kinds: {$sum : 1}} }
]);

// SCHEMA DESIGN =========================

// One-to-One relationship----------------
var owner = ObjectId();
db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact : "09876543210"
});

// paste the var and db.owners in robo then copy the object id and paste below in owner_id.
db.supplier.insert({
	name: "ABC Fruits",
	contact: "09998876543",
	owner_id: ObjectId("61777065f0b76e9bd3118707")
});

// update owner doc. and insert new field get the id of supplier
db.owners.updateOne({
	"_id": ObjectId("61777065f0b76e9bd3118707")},
	{$set:  {"supplier_id": ObjectId("6177717bf0b76e9bd3118708")}
	}
);

// One-to-Many relationship---------------
db.suppliers.insertMany([
		{
	"name": "DEF Fruits",
	"contact": "09632109847",
	"owner_id": ObjectId("61777065f0b76e9bd3118707")
		},
			{
		"name": "GHI Fruits",
		"contact": "09632107887",
		"owner_id": ObjectId("61777065f0b76e9bd3118707")
			}
]);
// paste code above in robo then copy the id provided in the robo nad paste here under supplier
db.owners.updateOne(
{"_id": ObjectId("61777065f0b76e9bd3118707")},
	{$set:  {"supplier_id": 
	[
	ObjectId("6177717bf0b76e9bd3118708"),
	ObjectId("61777a0ff0b76e9bd3118709"),
	ObjectId("61777a0ff0b76e9bd311870a")
	]}
	}
);




















